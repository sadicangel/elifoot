﻿using Elifoot.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    /// <summary>
    /// Defines the class SeasonRecord.
    /// </summary>
    /// <seealso cref="Elifoot.Util.ObservableDictionary{System.Int32, Elifoot.Source.SeasonResult}" />
    [Serializable]
    public class SeasonRecord : ObservableDictionary<int, SeasonResult>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SeasonRecord"/> class.
        /// </summary>
        public SeasonRecord() : base(new Dictionary<int, SeasonResult>()) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SeasonRecord"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        public SeasonRecord(IDictionary<int, SeasonResult> dictionary) : base(dictionary) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SeasonRecord"/> class.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        public SeasonRecord(IEqualityComparer<int> comparer) : base(comparer) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SeasonRecord"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="comparer">The comparer.</param>
        public SeasonRecord(IDictionary<int, SeasonResult> dictionary, IEqualityComparer<int> comparer) : base(dictionary, comparer) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SeasonRecord"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected SeasonRecord(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}

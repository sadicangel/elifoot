﻿using Elifoot.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    /// <summary>
    /// Definition of Player class.
    /// </summary>
    public class Player : INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// The name.
        /// </summary>
        private string _name;
        /// <summary>
        /// The country.
        /// </summary>
        private Country _country;
        /// <summary>
        /// The power.
        /// </summary>
        private int _power;
        /// <summary>
        /// The is star.
        /// </summary>
        private bool _isStar;
        /// <summary>
        /// The behaviour.
        /// </summary>
        private Behaviour _behaviour;
        /// <summary>
        /// The position.
        /// </summary>
        private Position _position;
        /// <summary>
        /// The is starter.
        /// </summary>
        private bool _isStarter;
        /// <summary>
        /// The is substitute.
        /// </summary>
        private bool _isSubstitute;
        /// <summary>
        /// The price.
        /// </summary>
        private int _price;
        /// <summary>
        /// The salary.
        /// </summary>
        private int _salary;
        /// <summary>
        /// The contract.
        /// </summary>
        private int _contract;
        /// <summary>
        /// The games.
        /// </summary>
        private int _games;
        /// <summary>
        /// The career goals.
        /// </summary>
        private int _careerGoals;
        /// <summary>
        /// The goals.
        /// </summary>
        private int _goals;
        /// <summary>
        /// The cards.
        /// </summary>
        private int _cards;
        /// <summary>
        /// The injuries.
        /// </summary>
        private int _injuries;
        /// <summary>
        /// The injury time.
        /// </summary>
        private int _injuryTime;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name { get => _name; set { _name = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public Country Country { get => _country; set { _country = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the power of the player.
        /// </summary>
        public int Power { get => _power; set { _power = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets a value indicating whether this player is a star.
        /// </summary>
        public bool IsStar { get => _isStar; set { _isStar = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the behaviour.
        /// </summary>
        public Behaviour Behaviour { get => _behaviour; set { _behaviour = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        public Position Position { get => _position; set { _position = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets a value indicating whether this player is starter.
        /// </summary>
        public bool IsStarter { get => _isStarter; set { _isStarter = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets a value indicating whether this player is substitute.
        /// </summary>
        public bool IsSubstitute { get => _isSubstitute; set { _isSubstitute = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public int Price { get => _price; set { _price = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the salary.
        /// </summary>
        public int Salary { get => _salary; set { _salary = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the contract (weeks).
        /// </summary>
        public int Contract { get => _contract; set { _contract = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the number of games.
        /// </summary>
        public int Games { get => _games; set { _games = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the number of career goals.
        /// </summary>
        public int CareerGoals { get => _careerGoals; set { _careerGoals = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the number of season goals.
        /// </summary>
        public int Goals { get => _goals; set { _goals = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the number of career cards.
        /// </summary>
        public int Cards { get => _cards; set { _cards = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the injuries.
        /// </summary>
        public int Injuries { get => _injuries; set { _injuries = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the injury time (weeks).
        /// </summary>
        public int InjuryTime { get => _injuryTime; set { _injuryTime = value; NotifyPropertyChanged(); NotifyPropertyChanged("IsInjured"); } }
        /// <summary>
        /// Gets a value indicating whether the player is injured.
        /// </summary>
        public bool IsInjured => InjuryTime > 0;
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}

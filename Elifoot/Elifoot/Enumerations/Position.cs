﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Enumerations
{
    /// <summary>
    /// Defines the position of a player.
    /// </summary>
    public enum Position
    {
        /// <summary>
        /// The goal keeper.
        /// </summary>
        [Description("G")]
        GoalKeeper,
        /// <summary>
        /// The defender.
        /// </summary>
        [Description("D")]
        Defender,
        /// <summary>
        /// The midfielder.
        /// </summary>
        [Description("M")]
        Midfielder,
        /// <summary>
        /// The forward.
        /// </summary>
        [Description("F")]
        Forward
    }
}

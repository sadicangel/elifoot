﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    public struct SeasonResult
    {
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Gets or sets the league winner team.
        /// </summary>
        public Club LeagueWinnerTeam { get; set; }
        /// <summary>
        /// Gets or sets the league winner coach.
        /// </summary>
        public Coach LeagueWinnerCoach { get; set; }
        /// <summary>
        /// Gets or sets the cup winner team.
        /// </summary>
        public Club CupWinnerTeam { get; set; }
        /// <summary>
        /// Gets or sets the cup winner coach.
        /// </summary>
        public Coach CupWinnerCoach { get; set; }
        /// <summary>
        /// Gets or sets the top scorer.
        /// </summary>
        public Player TopScorer { get; set; }
    }
}

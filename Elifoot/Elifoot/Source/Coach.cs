﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    public class Coach
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the number of games.
        /// </summary>
        public int Games { get; set; }
        /// <summary>
        /// Gets or sets the number of wins.
        /// </summary>
        public int Wins { get; set; }
        /// <summary>
        /// Gets or sets the number of draws.
        /// </summary>
        public int Draws { get; set; }
        /// <summary>
        /// Gets or sets the number of losses.
        /// </summary>
        public int Losses { get; set; }
        /// <summary>
        /// Gets or sets the record.
        /// </summary>
        public List<KeyValuePair<int, string>> Record { get; set; }
    }
}

﻿using Elifoot.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Interfaces
{
    /// <summary>
    /// Defines the interface that a company should implement.
    /// </summary>
    public interface ICompany
    {
        /// <summary>
        /// Does the annual review.
        /// </summary>
        void DoAnnualReview();

        /// <summary>
        /// Pays the balance due.
        /// </summary>
        void PayBalanceDue();

        /// <summary>
        /// Determines whether this instance [can pay loan] the specified quantity.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can pay loan] the specified quantity; otherwise, <c>false</c>.
        /// </returns>
        bool CanPayLoan(int quantity);

        /// <summary>
        /// Pays the loan.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        void PayLoan(int quantity);

        /// <summary>
        /// Determines whether this instance [can take loan] the specified quantity.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can take loan] the specified quantity; otherwise, <c>false</c>.
        /// </returns>
        bool CanTakeLoan(int quantity);

        /// <summary>
        /// Takes the loan.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        void TakeLoan(int quantity);

        /// <summary>
        /// Determines whether this instance [can build seats] the specified quantity.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can build seats] the specified quantity; otherwise, <c>false</c>.
        /// </returns>
        bool CanBuildSeats(int quantity);

        /// <summary>
        /// Builds the seats.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        void BuildSeats(int quantity);

        /// <summary>
        /// Sells the player.
        /// </summary>
        /// <param name="player">The player.</param>
        void SellPlayer(Player player);

        /// <summary>
        /// Buys the player.
        /// </summary>
        /// <param name="player">The player.</param>
        void BuyPlayer(Player player);
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Elifoot.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Elifoot.Source.Tests
{
    [TestClass()]
    public class ClubTests
    {
        [TestMethod()]
        public void ClubTest()
        {
            Club club = new Club(Squad.GetPlayers(15));
            Assert.IsInstanceOfType(club, typeof(Interfaces.ICompany));
            Assert.IsInstanceOfType(club, typeof(ObservableCollection<Player>));
            Assert.AreEqual(15, club.Count);
            Assert.IsNotNull(club.Stadium);
            Assert.IsNotNull(club.AnnualBalance);
            Assert.IsTrue(club.AnnualBalance.ContainsKey(DateTime.Now.Year));
            Assert.IsTrue(club.AnnualBalance.ContainsKey(DateTime.Now.Year - 1));
            Assert.IsTrue(club.AnnualBalance.ContainsKey(DateTime.Now.Year - 2));
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ClubFailTest()
        {
            new Club(null);
        }

        [TestMethod()]
        public void SelectSquadValidTeamTest()
        {
            Club club = new Club(Squad.GetPlayers(16));
            Squad squad = club.SelectSquad();
            Assert.IsTrue(squad.Count > 0);
        }

        [TestMethod()]
        public void SelectSquadInValidTeamTest()
        {
            Club club = new Club(Squad.GetPlayers(8, 2));
            Squad squad = club.SelectSquad();
            Assert.IsFalse(squad.Count > 0);
        }

        [TestMethod()]
        public void PaySalariesTest()
        {
            // Each player has a salary of 1000.
            const int amount = 15000;
            Club club = new Club(Squad.GetPlayers(15))
            {
                Capital = 30000
            };
            club.PaySalaries();
            Assert.AreEqual(amount, club.Capital);
            Assert.AreEqual(amount, club.AnnualBalance[DateTime.Now.Year].Salaries);

        }

        public void PaySalariesNegativeTest()
        {
            const int amount = -15000;
            Club club = new Club(Squad.GetPlayers(15))
            {
                Capital = 0
            };
            club.PaySalaries();
            Assert.AreEqual(amount, club.Capital);
            Assert.AreEqual(Math.Abs(amount), club.AnnualBalance[DateTime.Now.Year].Salaries);
        }

        [TestMethod()]
        public void PayInterestTest()
        {
            const int amount = 19000;
            Club club = new Club()
            {
                Capital = 20000,
                Debt = 10000
            };
            club.PayInterest();
            Assert.AreEqual(amount, club.Capital);
            Assert.AreEqual(1000, club.AnnualBalance[DateTime.Now.Year].Interest);
        }

        [TestMethod()]
        public void PayInterestNegativeTest()
        {
            const int amount = -1000;
            Club club = new Club()
            {
                Capital = 0,
                Debt = 10000
            };
            club.PayInterest();
            Assert.AreEqual(amount, club.Capital);
            Assert.AreEqual(1000, club.AnnualBalance[DateTime.Now.Year].Interest);
        }

        [TestMethod()]
        public void MakeStatementTest()
        {
            PaySalariesTest();
            PaySalariesNegativeTest();
            PayInterestTest();
            PaySalariesNegativeTest();
        }

        [TestMethod()]
        public void DoAnnualReviewTest()
        {
            Club club = new Club(Squad.GetPlayers(2)) { Capital = 20000 };
            Player player = new Player() { Price = 10000, Salary = 1000 };
            club[0].Price = 5000;
            club[1].Salary = 1000;
            club.SellPlayer(club[0]);
            club.BuyPlayer(player);
            club.TakeLoan(10000);
            club.PayBalanceDue();
            club.AnnualBalance[club.Year].Prizes += 2000;
            int year = DateTime.Now.Year;
            club.DoAnnualReview();
            Assert.IsFalse(club.AnnualBalance.ContainsKey(year - 2));
            Assert.IsTrue(club.AnnualBalance.ContainsKey(year - 1));
            Assert.IsTrue(club.AnnualBalance.ContainsKey(year));
            Assert.IsTrue(club.AnnualBalance.ContainsKey(year + 1));
            Assert.AreEqual(5000, club.AnnualBalance[year].Sales);
            Assert.AreEqual(10000, club.AnnualBalance[year].Purchases);
            Assert.AreEqual(1000, club.AnnualBalance[year].Interest);
            Assert.AreEqual(2000, club.AnnualBalance[year].Salaries);
            Assert.AreEqual(2000, club.AnnualBalance[year].Prizes);
            Assert.AreEqual(13000, club.AnnualBalance[year].Expenses);
            Assert.AreEqual(7000, club.AnnualBalance[year].Revenues);
        }

        [TestMethod()]
        public void PayLoanSuccessTest()
        {
            Club club = new Club()
            {
                Capital = 1000,
                Debt = 2500
            };
            Assert.IsTrue(club.CanPayLoan(1000));
            club.PayLoan(1000);
            Assert.IsFalse(club.CanPayLoan(1500));
        }

        [TestMethod()]
        public void TakeLoanTest()
        {
            Club club = new Club()
            {
                BankTrust = 0.9,
                Capital = 1000,
                Debt = 0
            };
            Assert.IsTrue(club.CanTakeLoan(1000 / 2));
            club.TakeLoan(1000 / 2);
            Assert.AreEqual(1500, club.Capital);
            Assert.IsTrue(club.CanTakeLoan(1000 / 2));
            club.TakeLoan(1000 / 2);
            Assert.AreEqual(2000, club.Capital);
            Assert.IsFalse(club.CanTakeLoan(1000 / 2));
        }

        [TestMethod()]
        public void BuildSeatsTest()
        {
            Club club = new Club()
            {
                Capital = 100000
            };
            Assert.IsTrue(club.CanBuildSeats(2000));
            club.BuildSeats(2000);
            Assert.AreEqual(2000, club.Stadium.Seats);
            Assert.IsFalse(club.CanBuildSeats(4000));
        }

        [TestMethod()]
        public void SellPlayerTest()
        {
            Club club = new Club(Squad.GetPlayers(1, 0))
            {
                Capital = 0
            };
            club[0].Price = 10000;
            club.SellPlayer(club[0]);
            Assert.AreEqual(10000, club.Capital);
            Assert.AreEqual(0, club.Count);
            Assert.AreEqual(10000, club.AnnualBalance[DateTime.Now.Year].Sales);
        }

        [TestMethod()]
        public void BuyPlayerTest()
        {
            Club club = new Club()
            {
                Capital = 10000
            };
            Player player = new Player() { Price = 10000 };
            club.BuyPlayer(player);
            Assert.AreEqual(0, club.Capital);
            Assert.AreEqual(1, club.Count);
            Assert.AreEqual(10000, club.AnnualBalance[DateTime.Now.Year].Purchases);
            // This actually only considers the reference.
            Assert.AreEqual(player, club.FirstOrDefault());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Enumerations
{
    /// <summary>
    /// Defines the type of formation of a team.
    /// </summary>
    public enum Formation
    {
        /// <summary>
        /// The 3 3 4 formation.
        /// </summary>
        [Description("3-3-4")]
        F_3_3_4,
        /// <summary>
        /// The 3 4 3 formation.
        /// </summary>
        [Description("3-4-3")]
        F_3_4_3,
        /// <summary>
        /// The 4 2 4 formation.
        /// </summary>
        [Description("4-2-4")]
        F_4_2_4,
        /// <summary>
        /// The 4 3 3 formation.
        /// </summary>
        [Description("4-3-3")]
        F_4_3_3,
        /// <summary>
        /// The 4 4 2 formation.
        /// </summary>
        [Description("4-4-2")]
        F_4_4_2,
        /// <summary>
        /// The 4 5 1 formation.
        /// </summary>
        [Description("4-5-1")]
        F_4_5_1,
        /// <summary>
        /// The 5 2 3 formation.
        /// </summary>
        [Description("5-2-3")]
        F_5_2_3,
        /// <summary>
        /// The 5 3 2 formation.
        /// </summary>
        [Description("5-3-2")]
        F_5_3_2,
        /// <summary>
        /// The 5 4 1 formation.
        /// </summary>
        [Description("5-4-1")]
        F_5_4_1,
        /// <summary>
        /// The 5 5 0 formation.
        /// </summary>
        [Description("5-5-0")]
        F_5_5_0,
        /// <summary>
        /// The 6 3 1 formation.
        /// </summary>
        [Description("6-3-1")]
        F_6_3_1,
        /// <summary>
        /// The 6 4 0 formation.
        /// </summary>
        [Description("6-4-0")]
        F_6_4_0,
        /// <summary>
        /// The automatic formation.
        /// </summary>
        [Description("Automatic")]
        F_Auto,
        /// <summary>
        /// The best formation.
        /// </summary>
        [Description("The Best")]
        F_Best
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    public class Balance
    {
        /// <summary>
        /// Gets or sets the tickets.
        /// </summary>
        public int Tickets { get; set; }
        /// <summary>
        /// Gets or sets the sales.
        /// </summary>
        public int Sales { get; set; }
        /// <summary>
        /// Gets or sets the prizes.
        /// </summary>
        public int Prizes { get; set; }
        /// <summary>
        /// Gets or sets the salaries.
        /// </summary>
        public int Salaries { get; set; }
        /// <summary>
        /// Gets or sets the purchases.
        /// </summary>
        public int Purchases { get; set; }
        /// <summary>
        /// Gets or sets the interest.
        /// </summary>
        public int Interest { get; set; }
        /// <summary>
        /// Gets the revenues.
        /// </summary>
        public int Revenues { get { return Tickets + Sales + Prizes; } }
        /// <summary>
        /// Gets the expenses.
        /// </summary>
        public int Expenses { get { return Salaries + Purchases + Interest; } }
        /// <summary>
        /// Gets the outcome.
        /// </summary>
        public int Outcome { get { return Revenues - Expenses; } }
    }
}

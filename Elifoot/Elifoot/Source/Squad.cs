﻿using Elifoot.Enumerations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    /// <summary>
    /// Definition of Squad class.
    /// </summary>
    public class Squad : ObservableCollection<Player>
    {
        #region Constants
        /// <summary>
        /// The number of starters.
        /// </summary>
        public const int NumberOfStarters = 11;
        /// <summary>
        /// The number of substitutes.
        /// </summary>
        public const int NumberOfSubstitutes = 5;
        #endregion

        #region Fields        
        /// <summary>
        /// The formation.
        /// </summary>
        private Formation _formation;
        /// <summary>
        /// The is home squad.
        /// </summary>
        private bool _isHomeSquad;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the formation.
        /// </summary>
        public Formation Formation { get => _formation; set { _formation = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is home squad.
        /// </summary>
        public bool IsHomeSquad { get => _isHomeSquad; set { _isHomeSquad = value; NotifyPropertyChanged(); } }
        /// <summary>
        /// Gets or sets the starters.
        /// </summary>
        public IEnumerable<Player> Starters { get { return this.Where(p => p.IsStarter); } }
        /// <summary>
        /// Gets or sets the substitutes.
        /// </summary>
        public IEnumerable<Player> Substitutes { get { return this.Where(p => p.IsSubstitute); } }
        /// <summary>
        /// Returns true if the collection of Player is a valid Squad.
        /// </summary>
        /// <param name="players">The players.</param>
        public static bool IsValid(IEnumerable<Player> players) { return players != null && players.Count(p => p.IsStarter) == NumberOfStarters && players.Count(p => p.IsSubstitute) < NumberOfSubstitutes && players.Count(p => p.Position == Position.GoalKeeper) == 1; }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Squad"/> class.
        /// </summary>
        public Squad() : base() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="Squad"/> class.
        /// </summary>
        /// <param name="list">The list from which the elemets are copied.</param>
        public Squad(IEnumerable<Player> list) : base(list) { }
        #endregion

        #region Private Methods
        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
           OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Gets a list of players where the first <see cref="Squad.NumberOfStarters"/>
        /// are starters and the rest are substitutes. Can specify how many are GoalKeepers.
        /// </summary>
        /// <param name="count">The count.</param>
        /// <param name="gk">The gk.</param>
        /// <returns></returns>
        public static IEnumerable<Player> GetPlayers(int count, int gk = 1)
        {
            List<Player> list = new List<Player>(count);
            for (int i = 0; i < count; ++i)
            {
                list.Add(new Player() {
                    Position = gk > i ? Position.GoalKeeper : Position.Forward,
                    IsStarter = i < NumberOfStarters,
                    IsSubstitute = i > NumberOfStarters,
                    Salary = 1000,
                });
            }
            return list;
        }
        #endregion
    }
}

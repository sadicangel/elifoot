﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Elifoot.Source
{
    /// <summary>
    /// Definition of Country class.
    /// </summary>
    public class Country
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the flag.
        /// </summary>
        public string Flag { get; set; }
        /// <summary>
        /// Gets or sets the color of the main.
        /// </summary>
        public Color MainColor { get; set; }
        /// <summary>
        /// Gets or sets the color of the back.
        /// </summary>
        public Color BackColor { get; set; }
    }
}

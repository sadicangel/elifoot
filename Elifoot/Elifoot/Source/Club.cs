﻿using Elifoot.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Elifoot.Source
{
    /// <summary>
    /// Definition of Club class.
    /// </summary>
    public class Club : ObservableCollection<Player>, ICompany
    {
        #region Properties
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the main color.
        /// </summary>
        public Color MainColor { get; set; }
        /// <summary>
        /// Gets or sets the background color.
        /// </summary>
        public Color BackColor { get; set; }
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public Country Country { get; set; }
        /// <summary>
        /// Gets or sets the stadium.
        /// </summary>
        public Stadium Stadium { get; set; }
        /// <summary>
        /// Gets or sets the morale.
        /// </summary>
        public int Morale { get; set; }
        /// <summary>
        /// Gets or sets the number of games.
        /// </summary>
        public int Games { get; set; }
        /// <summary>
        /// Gets or sets the number of wins.
        /// </summary>
        public int Wins { get; set; }
        /// <summary>
        /// Gets or sets the number of draws.
        /// </summary>
        public int Draws { get; set; }
        /// <summary>
        /// Gets or sets the number of losses.
        /// </summary>
        public int Losses { get; set; }
        /// <summary>
        /// Gets or sets the bank trust.
        /// </summary>
        public double BankTrust { get; set; }
        /// <summary>
        /// Gets or sets the capital.
        /// </summary>
        public int Capital { get; set; }
        /// <summary>
        /// Gets or sets the debt.
        /// </summary>
        public int Debt { get; set; }
        /// <summary>
        /// Gets or sets the annual balance.
        /// </summary>
        public AnnualBalance AnnualBalance { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Squad"/> class.
        /// </summary>
        public Club() : base()
        {
            Year = DateTime.Now.Year;
            Stadium = new Stadium();
            AnnualBalance = new AnnualBalance(DateTime.Now.Year);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Squad"/> class.
        /// </summary>
        /// <param name="list">The list from which the elements are copied.</param>
        public Club(IEnumerable<Player> list) : base(list)
        {
            Year = DateTime.Now.Year;
            Stadium = new Stadium();
            AnnualBalance = new AnnualBalance(DateTime.Now.Year);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Selects the squad.
        /// </summary>
        /// <returns></returns>
        public Squad SelectSquad()
        {
            var players = this.Where(p => p.IsStarter || p.IsSubstitute);
            if (Squad.IsValid(players)) return new Squad(players);
            return new Squad();
        }
        /// <summary>
        /// Pays the salaries.
        /// </summary>
        public void PaySalaries()
        {
            int salaries = this.Sum(p => p.Salary);
            Capital -= salaries;
            if (Capital < 0) BankTrust -= 0.1;
            AnnualBalance[Year].Salaries += salaries;
        }

        /// <summary>
        /// Pays the interest.
        /// </summary>
        public void PayInterest()
        {
            int interest = Debt / 10;
            Capital -= interest;
            AnnualBalance[Year].Interest += interest;
        }

        /// <summary>
        /// Does the annual review.
        /// </summary>
        public void DoAnnualReview()
        {
            Year = Year + 1;
            AnnualBalance.Year = Year;
        }

        /// <summary>
        /// Pays the balance due.
        /// </summary>
        public void PayBalanceDue()
        {
            PaySalaries();
            PayInterest();
        }

        /// <summary>
        /// Determines whether this instance [can pay loan] the specified quantity.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can pay loan] the specified quantity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanPayLoan(int quantity)
        {
            return Capital >= quantity;
        }

        /// <summary>
        /// Pays the loan.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        public void PayLoan(int quantity)
        {
            BankTrust += quantity / 10000D;
            Capital -= quantity;
            Debt -= quantity;
        }

        /// <summary>
        /// Determines whether this instance [can take loan] the specified quantity.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can take loan] the specified quantity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanTakeLoan(int quantity)
        {
            return Capital * BankTrust >= quantity + Debt * 1 / BankTrust;
        }

        /// <summary>
        /// Takes the loan.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        public void TakeLoan(int quantity)
        {
            BankTrust -= quantity / 10000D;
            Capital += quantity;
            Debt += quantity;
        }

        /// <summary>
        /// Determines whether this instance [can build seats] the specified quantity.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can build seats] the specified quantity; otherwise, <c>false</c>.
        /// </returns>
        public bool CanBuildSeats(int quantity)
        {
            return Capital >= Stadium.PriceOfSeat * quantity;
        }

        /// <summary>
        /// Builds the seats.
        /// </summary>
        /// <param name="quantity">The quantity.</param>
        public void BuildSeats(int quantity)
        {
            Stadium.Seats += quantity;
            Capital -= quantity * Stadium.PriceOfSeat;
        }

        /// <summary>
        /// Sells the player.
        /// </summary>
        /// <param name="player">The player.</param>
        public void SellPlayer(Player player)
        {
            Capital += player.Price;
            AnnualBalance[Year].Sales += player.Price;
            Remove(player);
        }

        /// <summary>
        /// Buys the player.
        /// </summary>
        /// <param name="player">The player.</param>
        public void BuyPlayer(Player player)
        {
            Capital -= player.Price;
            AnnualBalance[Year].Purchases = player.Price;
            Add(player);
        }
        #endregion
    }
}

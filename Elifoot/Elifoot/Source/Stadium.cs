﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    public class Stadium
    {
        #region Constants
        /// <summary>
        /// The price of a seat.
        /// </summary>
        public const int PriceOfSeat = 20;
        #endregion
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the number of seats.
        /// </summary>
        public int Seats { get; set; }
        /// <summary>
        /// Gets or sets the ticket price.
        /// </summary>
        public int TicketPrice { get; set; }
    }
}

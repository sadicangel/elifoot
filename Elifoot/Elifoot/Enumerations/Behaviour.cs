﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Enumerations
{
    /// <summary>
    /// Defines what type of behaviour a player has.
    /// </summary>
    public enum Behaviour
    {
        /// <summary>
        /// The sanguinary player.
        /// </summary>
        [Description("Sanguinary")]
        Sanguinary,
        /// <summary>
        /// The thug player.
        /// </summary>
        [Description("Thug")]
        Thug,
        /// <summary>
        /// The rough player.
        /// </summary>
        [Description("Rough")]
        Rough,
        /// <summary>
        /// The gentleman player.
        /// </summary>
        [Description("Gentleman")]
        Gentleman,
        /// <summary>
        /// The harmless player.
        /// </summary>
        [Description("Harmless")]
        Harmless,
        /// <summary>
        /// The fair player.
        /// </summary>
        [Description("Fair Play")]
        FairPlay,
    }
}

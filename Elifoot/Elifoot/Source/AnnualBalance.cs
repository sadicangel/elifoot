﻿using Elifoot.Util;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Elifoot.Source
{
    /// <summary>
    /// Definition of class AnnualBalance.
    /// </summary>
    /// <seealso cref="Elifoot.Util.ObservableDictionary{System.Int32, Elifoot.Source.Balance}" />
    [Serializable]
    public class AnnualBalance : ObservableDictionary<int, Balance>
    {
        #region Fields
        /// <summary>
        /// The year.
        /// </summary>
        private int _year;
        #endregion

        #region Properties        
        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public int Year { get => _year; set { _year = value; DoReview(value); NotifyPropertyChanged(); } }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="AnnualBalance"/> class.
        /// </summary>
        public AnnualBalance() : base(new Dictionary<int, Balance>() { { DateTime.Now.Year - 2, new Balance() }, { DateTime.Now.Year - 1, new Balance() }, { DateTime.Now.Year, new Balance() } }) { Year = DateTime.Now.Year; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnualBalance"/> class.
        /// </summary>
        /// <param name="year">The year.</param>
        public AnnualBalance(int year) : base(new Dictionary<int, Balance>() { { year - 2, new Balance() }, { year - 1, new Balance() }, { year, new Balance() } }) { Year = year; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnualBalance"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        public AnnualBalance(IDictionary<int, Balance> dictionary) : base(dictionary) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnualBalance"/> class.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        public AnnualBalance(IEqualityComparer<int> comparer) : base(comparer) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnualBalance"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="comparer">The comparer.</param>
        public AnnualBalance(IDictionary<int, Balance> dictionary, IEqualityComparer<int> comparer) : base(dictionary, comparer) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnualBalance"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected AnnualBalance(SerializationInfo info, StreamingContext context) : base(info, context) { }
        #endregion

        #region Private Methods
        /// <summary>
        /// Does the review.
        /// </summary>
        /// <param name="year">The year.</param>
        private void DoReview(int year)
        {
            if (!ContainsKey(year))
            {
                Add(year, new Balance());
                Remove(year - 3);
            }
        }

        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            OnPropertyChanged(propertyName);
        }
        #endregion
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Elifoot.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Elifoot.Source.Tests
{
    [TestClass()]
    public class SquadTests
    {
        [TestMethod()]
        public void IsValidSuccessTest()
        {
            IEnumerable<Player> players = Squad.GetPlayers(14);
            Assert.IsTrue(Squad.IsValid(players));
            Squad squad = new Squad(players);
            Assert.IsTrue(Squad.IsValid(squad));
        }

        [TestMethod()]
        public void IsValidSuccessMinPlayersTest()
        {
            Assert.IsTrue(Squad.IsValid(Squad.GetPlayers(11)));
        }

        [TestMethod()]
        public void IsValidSuccessMaxPlayersTest()
        {
            Assert.IsTrue(Squad.IsValid(Squad.GetPlayers(16)));
        }

        [TestMethod()]
        public void IsValidFailNullOrEmptyTest()
        {
            Assert.IsFalse(Squad.IsValid(null));
            Assert.IsFalse(Squad.IsValid(new Squad()));
        }

        [TestMethod()]
        public void IsValidFailMaxPlayersTest()
        {
            Assert.IsFalse(Squad.IsValid(Squad.GetPlayers(17)));
        }

        [TestMethod()]
        public void IsValidFailMinPlayersTest()
        {
            Assert.IsFalse(Squad.IsValid(Squad.GetPlayers(10)));
        }

        [TestMethod()]
        public void IsValidFailNoGoalKeeperTest()
        {
            Assert.IsFalse(Squad.IsValid(Squad.GetPlayers(14, 0)));
        }

        [TestMethod()]
        public void IsValidMoreThanOneGoalKeeperTest()
        {
            Assert.IsFalse(Squad.IsValid(Squad.GetPlayers(14, 2)));
        }

        [TestMethod()]
        public void SquadSuccessTest()
        {
            Squad squad = new Squad(Squad.GetPlayers(14));
            Assert.IsNotNull(squad);
            Assert.IsInstanceOfType(squad, typeof(ObservableCollection<Player>));

        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SquadSuccessNullListTest()
        {
            new Squad(null);
        }

        [TestMethod()]
        public void SquadSuccessWithNoElementsTest()
        {
            Assert.IsNotNull(new Squad());
        }

        [TestMethod()]
        public void SquadSuccessWithEmptyListTest()
        {
            Assert.IsNotNull(new Squad(new List<Player>(2)));
        }
    }
}